from view_obj import View
from model import Ensemble
from model import Person


class Controller:
    """
    Initialization of controller class
    """
    def __init__(self):
        """
        Initialization of the view and the model
        """
        self.view = View(self)
        self.model = Ensemble()

    def start_view(self):
        """
        Creation of the view
        """
        self.view.create_fields()
        self.view.main()

    def search(self):
        """
        Search a person in the dictionary
        """
        person = Person(self.view.get_value("Nom"),
                        self.view.get_value("Prenom"),
                        self.view.get_value("Telephone"),
                        self.view.get_value("Adresse"),
                        self.view.get_value("Ville"))
        print(person)

        self.model.search_person(person)

    def delete(self):
        """
        Erase a person from the dictionary
        """
        person = Person(self.view.get_value("Nom"),
                        self.view.get_value("Prenom"),
                        self.view.get_value("Telephone"),
                        self.view.get_value("Adresse"),
                        self.view.get_value("Ville"))

        self.view.delete_value("Nom"),
        self.view.delete_value("Prenom"),
        self.view.delete_value("Telephone"),
        self.view.delete_value("Adresse"),
        self.view.delete_value("Ville")

        print(person)
        self.model.delete_person(person)

    def insert(self):
        """
        Insert a person from into the dictionary
        """
        person = Person(self.view.get_value("Nom"),
                        self.view.get_value("Prenom"),
                        self.view.get_value("Telephone"),
                        self.view.get_value("Adresse"),
                        self.view.get_value("Ville"))
        print(person)

        self.model.insert_person(person)

    def button_press_handle(self, buttonId):
        """
        Action link the to the press of a button
        """
        print("[Controller][button_press_handle] " + buttonId)
        if buttonId == "Chercher":
            self.search()
        elif buttonId == "Effacer":
            self.delete()
            self.view.delete_msg()
        elif buttonId == "Inserer":
            self.insert()
            self.view.insert_msg()
        elif buttonId == "Quitter":
            self.view.quit_message()
        else:
            pass


if __name__ == "__main__":
    controller = Controller()
    controller.start_view()

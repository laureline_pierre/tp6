# coding: utf-8

import os
from tkinter import Button, END, messagebox
from tkinter import Entry
from tkinter import filedialog
from tkinter import Label
from tkinter import Menu
from tkinter import StringVar
from tkinter import Tk
import tkinter as tk
from tkinter.messagebox import askokcancel
from tkinter.messagebox import askyesno

"""
Class View of the project

@author : Olivier CHABROL
"""


class View(tk.Tk):
    """
    Construction of the view
    """
    def __init__(self, controller):
        """
        Initialization of the different objects in the view
        """
        super().__init__()
        self.controller = controller
        self.widgets_labs = {}
        self.widgets_entry = {}
        self.widgets_button = {}
        self.entries = ["Nom", "Prenom", "Telephone", "Adresse", "Ville"]
        self.buttons = ["Chercher", "Inserer", "Effacer", "Quitter"]
        self.modelListFields = []
        self.fileName = None
        self.windows = {"fenetreResult": ..., "fenetreErreur": ...}

    def get_value(self, key):
        """
        Return the value of a specific entry
        """
        return self.widgets_entry[key].get()

    def delete_value(self, key):
        """
        Delete all the values in all entries put by the user
        """
        return self.widgets_entry[key].delete(0, END)

    def delete_msg(self):
        """
        Display a pop-up asking the user if he/her really want to delete a contact
        """
        return askyesno("Erase a contact", "Do you want to erase the contact ?")

    def insert_msg(self):
        """
        Display a pop-up asking the user if he/her really want to insert a contact
        """
        return askyesno("Insert a contact", "Do you want to insert the contact ?")

    def quit_message(self):
        """
        Display a pop-up asking the user if he/her really want to quit the GUI
        """
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            self.destroy()

    def create_fields(self):
        """
        Creation of the labels, entries and buttons on the GUI
        """
        i, j = 0, 0

        for idi in self.entries:
            lab = Label(self, text=idi.capitalize())
            self.widgets_labs[idi] = lab
            lab.grid(row=i, column=0)

            var = StringVar()
            entry = Entry(self, text=var)
            self.widgets_entry[idi] = entry
            entry.grid(row=i, column=1)

            i += 1

        for idi in self.buttons:
            buttonW = Button(self, text=idi, command=(lambda button=idi: self.controller.button_press_handle(button)))
            self.widgets_button[idi] = buttonW
            buttonW.grid(row=i + 1, column=j)
            #self.widgets_button[idi].config(command = idi)

            j += 1

    def main(self):
        """
        Display the interface
        """
        print("[View] main")
        self.title("Annuaire")
        self.mainloop()
